package u02

object BTrees extends App {

  // A custom and generic binary tree of elements of type A
  sealed trait Tree[A]
  object Tree {

    case class Leaf[A](value: A) extends Tree[A]

    case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

    private def traversal[A, B](tree: Tree[A])(behavior: A => B)(op: (B, B) => B): B = tree match {
      case Branch(l, r) => op(traversal(l)(behavior)(op), traversal(r)(behavior)(op))
      case Leaf(v) => behavior(v)
    }

    def count[A](tree: Tree[A], elem: A): Int = traversal(tree)((a: A) => a == elem match {
      case true => 1
      case _ => 0
    })(_ + _)

    def find[A](tree: Tree[A], elem: A): Boolean = traversal(tree)((a: A) => a == elem)(_ || _)
    def size[A](tree: Tree[A]): Int = traversal(tree)(_ => 1)(_ + _)
  }

  import Tree._
  val tree = Branch(Branch(Leaf(1), Leaf(2)), Leaf(1))
  println(tree, size(tree)) // ..,3
  println(find(tree, 1)) // true
  println(find(tree, 4)) // false
  println(count(tree, 1)) // 2
}
