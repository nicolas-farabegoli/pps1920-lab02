package u02.ex3

object Functions extends App {

  def parity(elem: Int): String = elem % 2 match {
    case 0 => "Even"
    case _ => "Odd"
  }

  val parityFl: Int => String = _ % 2 match {
    case 0 => "Even"
    case _ => "Odd"
  }

  println(parityFl(10))
  println(parity(11))

  def neg[A](predicate: A => Boolean): A => Boolean = !predicate(_)

  val notEquals = neg[Int](_ == 7)
  val notEmpty = neg[String](_ == "")

  println(notEquals(7))
  println(notEquals(8))
  println(notEmpty(""))
  println(notEmpty("foobar"))
}
