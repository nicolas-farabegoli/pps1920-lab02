package u02.ex4

object Currying extends App {

  // standard function with no currying
  def mult(x: Double, y: Double): Double = x*y

  // function with currying
  // curriedMult has actually type: Double => (Double => Double)
  def curriedMult(x: Double)(y: Double): Double = x*y

  // slightly different syntax at the call side..
  println(mult(10,2)) // 20
  println(curriedMult(10)(2)) // 20

  // curriedMult can be partially applied!
  val twice: Double => Double = curriedMult(2)

  println(twice(10)) // 20

  // => is right-associative, hence it is equal to:
  //val curr...: Double => (Double => Double) = x => (y => x*y)
  val curriedMultAsFunction: Double => Double => Double = x => y => x*y

  println(curriedMultAsFunction(10)(2)) // 20
  println(curriedMultAsFunction) // u02.Currying$$$Lambda$7/...
  println(curriedMultAsFunction(10)) // u02.Currying$$$Lambda$12/...

  val p1: Double => Double => Double => Boolean = x => y => z => x <= y && y <= z
  val p2 = (x: Double, y: Double, z: Double) => x <= y && y <= z
  def p3(x: Double)(y: Double)(z: Double): Boolean = x <= y && y <= z
  def p4(x: Double, y: Double, z: Double): Boolean = x <= y && y <= z
  val partial: Double => Boolean = p3(2)(3)

  println(p1(6)(4)(5))
  println(p2(1, 4, 5))
  println(p3(6)(4)(5))
  println(p4(6, 4, 5))
  println(partial(1))
}
