package u02.ex5

object Compose extends App {

  def compose[A](f: A => A, g: A => A): A => A = {
     i: A => f(g(i))
  }

  println(compose[Int](_-1, _*2)(5))
  println(compose[Double](_-1, _*2.5)(10))
  println(compose[String](_.concat(" last"), _.replaceAll("t", "l"))("Hetto"))
}
