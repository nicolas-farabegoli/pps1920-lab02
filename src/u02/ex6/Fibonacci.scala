package u02.ex6

object Fibonacci extends App {

  def fib(n: Int): Int = {
    @scala.annotation.tailrec
    def fibTailRec(n: Int, prev: Int, cur: Int): Int = n match {
      case 0 => prev
      case 1 => cur
      case _ => fibTailRec(n-1, cur, prev+cur)
    }
    fibTailRec(n, 0, 1)
  }

  println(fib(0))
  println(fib(1))
  println(fib(2))
  println(fib(3))
  println(fib(4))
  println(fib(5))
  println(fib(6))
}
