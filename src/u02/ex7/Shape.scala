package u02.ex7

object Shape extends App {

  sealed trait Shape
  object Shape {
    case class Rectangle(w: Double, h: Double) extends Shape
    case class Circle(r: Double) extends Shape
    case class Square(l: Double) extends Shape

    def perimeter(s: Shape): Double = s match {
      case Rectangle(x, y) => 2*x + 2*y
      case Circle(x) => 2*Math.PI*x
      case Square(x) => 4*x
    }

    def area(s: Shape): Double = s match {
      case Rectangle(x, y) => x*y
      case Circle(x) => x*x*Math.PI
      case Square(x) => x*x
    }
  }

}
