package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class FunctionsTest {
  import u02.ex3.Functions._

  @Test def testEqualInteger(): Unit = {
    val intNegate = neg[Int](_ == 10)
    assertTrue(intNegate(12))
    assertFalse(intNegate(10))
  }

  @Test def testGraterThanInteger(): Unit = {
    val intNegate = neg[Int](_ > 5)
    assertTrue(intNegate(4))
    assertFalse(intNegate(10))
  }

  @Test def testStringNegate(): Unit = {
    val stringNegate = neg[String](_ == "Hello")
    assertFalse(stringNegate("Hello"))
    assertTrue(stringNegate(""))
  }
}
