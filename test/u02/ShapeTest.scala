package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ShapeTest {

  import u02.ex7.Shape.Shape._

  val rectangle: Rectangle = Rectangle(12.0, 2.5)
  val circle: Circle = Circle(12.3)
  val square: Square = Square(23.0)

  val DELTA: Double = 1e-4

  @Test def rectangleAreaTest(): Unit = {
    assertEquals(30, area(rectangle))
  }

  @Test def circleAreaTest(): Unit = {
    assertEquals(475.291552562, area(circle), DELTA)
  }

  @Test def squareAreaTest(): Unit = {
    assertEquals(529, area(square))
  }

  @Test def rectanglePerimeterTest(): Unit = {
    assertEquals(29.0, perimeter(rectangle))
  }

  @Test def circlePerimeterTest(): Unit = {
    assertEquals(77.2831792783, perimeter(circle), DELTA)
  }

  @Test def squarePerimeterTest(): Unit = {
    assertEquals(92.0, perimeter(square))
  }
}
